const path = require('path');

module.exports = {
	entry: './app/index.js',
	output: {
		path: __dirname,
		filename: 'bundle.js',
		publicPath: '/app/assets/'
	},
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: 'babel-loader',
				include: path.join(__dirname, 'app'),
				exclude: /node_modules/,
				options: {
					presets: ['react', 'es2015', 'stage-1'],
					plugins: [
						'transform-es2015-parameters',
						'transform-es2015-destructuring'
					]
				}
			}
		]
	},

};
