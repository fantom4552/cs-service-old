preact
preact-router
preact-async-route
preact-render-to-string 



 - http://redux.js.org/  
 - https://material-ui-1dab0.firebaseapp.com  
 - http://docs.sequelizejs.com/manual/installation/getting-started.html  
 - https://reacttraining.com/react-router/web/example/basic  
 - https://www.npmjs.com/package/bcrypt-nodejs  
 - https://stackoverflow.com/questions/27835801/how-to-auto-generate-migrations-with-sequelize-cli-from-sequelize-models  
 - https://blog.jscrambler.com/implementing-jwt-using-passport/  
 - https://jonathanmh.com/express-passport-json-web-token-jwt-authentication-beginners/  
 - https://www.npmjs.com/package/sequelize-json  
 - https://www.npmjs.com/package/express-acl  

```
admins
	id
	nickname
	authId
	password
	flags
servers
	id
	address
	title
admins_servers
	adminId
	serverId
	accessId
	expired
servers_access
	id
	serverId
	title
	access
users
	id
	username
	password
	email
	status [unconfirmed, registered, banned]
roles
	id
	title
	access
roles_servers
	roleId
	serverId
	access
roles_users
	roleId
	userId
```
