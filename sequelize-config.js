require('dotenv').config();
const config = require('config');
const cfg = {};
cfg[process.env.NODE_ENV] = {
	username: config.get('DataBase.user'),
	password: config.get('DataBase.pass'),
	database: config.get('DataBase.name'),
	host: config.get('DataBase.params.host'),
	port: config.get('DataBase.params.port'),
	dialect: config.get('DataBase.params.dialect')
};
module.exports = cfg;
