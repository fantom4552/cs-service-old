require('dotenv').config();

const app = require('./src/app');

const config = require('config');

const LoggerHelper = require('./src/helpers/LoggerHelper');

// const routePath = require.resolve('express/lib/router/route');
// const route = require.cache[routePath].exports;
// const oldDispatch = route.prototype.dispatch;
// route.prototype.dispatch = (req, res, done) => {
// 	console.log(req);
//     return oldDispatch.call(this, req, res, done);
// };


app.listen(config.get('App.port'), () => {
	LoggerHelper.debug(`App listening on port ${config.get('App.port')}!`);
});
