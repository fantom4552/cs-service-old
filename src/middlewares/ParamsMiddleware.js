const AdminModel = require('../models').AdminModel;
const ServerModel = require('../models').ServerModel;

const RouteParamAdminId = (req, res, next, id) => {
    AdminModel
		.findById(id)
		.then(admin => {
			if (admin) {
				req.admin = admin;
				next();
			} else {
				next(new Error('Not found'));
			}
		});
};

const RouteParamServerId = (req, res, next, id) => {
    ServerModel
		.findById(id)
		.then(server => {
			if (server) {
				req.server = server;
				next();
			} else {
				next(new Error('Not found'));
			}
		});
};

module.exports = {
	RouteParamAdminId: (router) => {
		router.param('adminId', RouteParamAdminId);
	},
	RouteParamServerId: (router) => {
		router.param('serverId', RouteParamServerId);
	}
};
