const SecurityHelper = require('../helpers/SecurityHelper');

const CheckContentType = (req, res, next) => {
    if (req.method === 'GET' || req.method === 'DELETE') {
        return next();
    }

    if (req.get('Content-Type') === 'application/json') {
        return next();
    }

    return next(new Error('Bad Content-Type'));
};

const CheckToken = async (req, res, next) => {
    try {
        const token = (req && req.cookies && req.cookies.jwt) ? req.cookies.jwt : null;
        if (!token) {
            req.user = null;
            return next();
        }

        const data = await SecurityHelper.tokenVerify(token, process.env.JWT_SECRET);
        req.user = {
            id: data.id
		};
        return next();
    } catch (error) {
        return next(error);
    }
};

const checkAuth = (req, res, next) => {

    // if (!req.user) {
    //     return next(new Error('Not authorized'));
    // }

    return next();
};

module.exports = {
    CheckContentType,
    CheckToken,
	checkAuth
};
