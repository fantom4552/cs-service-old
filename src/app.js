
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const SecurityMiddleware = require('./middlewares/SecurityMiddleware');

const usersRoutes = require('./routes/users');
const adminsRoutes = require('./routes/admins');
const serversRoutes = require('./routes/servers');

const app = express();

app.disable('x-powered-by');
app.set('json replacer', (key, value) => {
	return (key !== 'password') ? value : undefined;
});

app.use(bodyParser.json());
app.use(cookieParser());

if (process.env.NODE_ENV === 'dev') {
	const webpack = require('webpack');
	const webpackDevMiddleware = require('webpack-dev-middleware');
	const webpackHotMiddleware = require('webpack-hot-middleware');
	const webpackConfig = require('../webpack.config.js');
	const compiler = webpack(webpackConfig);
	app.use(webpackHotMiddleware(compiler));
	app.use(webpackDevMiddleware(compiler, {
		noInfo: true,
	}));
}

app.use(SecurityMiddleware.CheckContentType);
app.use(SecurityMiddleware.CheckToken);

const API_ENDPOINT = '/api/v1';
app.use(usersRoutes);
app.use(`${API_ENDPOINT}/admins`, adminsRoutes);
app.use(`${API_ENDPOINT}/servers`, serversRoutes);

const fs = require('fs');
const indexHTML = fs.readFileSync(`${__dirname}/../index.html`, 'utf8').toString();

app.get('*', (req, res) => {
	res.send(indexHTML.replace('{USER_DATA}', JSON.stringify(req.user)));
});

app.use(function (error, req, res, next) {
	res
		.status(error.status || 500)
		.json({
			error: error.message
		})
		.end();
	console.log(error)
});

module.exports = app;
