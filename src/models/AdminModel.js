const crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('AdminModel', {
		id: {
			allowNull: false,
			autoIncrement: false,
			primaryKey: true,
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
		},
		nickname: {
			type: DataTypes.STRING(32),
		},
		authId: {
			type: DataTypes.STRING(22),
			unique: true,
			validate: {
				isUnique: (value, next) => {
					sequelize.models.AdminModel
						.count({
							where: {
								authId: value
							}
						})
						.then((count) => {
							if (count > 0) {
								return next('admin already exists');
							}
							return next();
						})
						.catch(function (err) {
							return next(err);
						});
				}
			}
		},
		password: {
			type: DataTypes.STRING(40),
			set(val) {
				if (val !== null) {
					this.setDataValue('password', crypto.createHash('md5').update(val).digest("hex"));
				} else {
					this.setDataValue('password', null);
				}
			}
		},
		flags: {
			type: DataTypes.INTEGER(11).UNSIGNED,
			defaultValue: 0,
			allowNull: true,
		}
	}, {
		timestamps: true,
		underscored: false,
		tableName: 'admins'
	});
};
