const _ = require('lodash');
const sequelize = require('../helpers/DatabaseHelper');
const SecurityHelper = require('../helpers/SecurityHelper');

const UserModel = sequelize.import('./UserModel');
const AccountModel = sequelize.import('./AccountModel');
const AdminModel = sequelize.import('./AdminModel');
const ServerModel = sequelize.import('./ServerModel');
const AdminServerModel = sequelize.import('./AdminServerModel');
const UserAccountModel = sequelize.import('./UserAccountModel');

UserModel.addHook('beforeCreate', 'passwordHook', (instance) => {
	if (!instance.changed('password')) {
		return instance;
	}
	return SecurityHelper.passwordHash(instance.get('password'))
		.then(hash => {
			instance.password = hash;
			return instance;
		})
});

// User -> 1:1 -> Account owner
UserModel.belongsTo(AccountModel, {
    as: 'owner',
    foreignKey: 'ownerId',
    constraints: false,
});

// Account -> 1:n -> Server
AccountModel.belongsTo(ServerModel, {
    as: 'account',
    foreignKey: 'accountId',
    constraints: false,
});

// User -> n:n (UserAccount) -> Account
UserModel.belongsToMany(AccountModel, {
    as: 'accounts',
    through: UserAccountModel,
    foreignKey: 'userId',
    constraints: false,
});

// Account -> n:n (UserAccount) -> User
AccountModel.belongsToMany(UserModel, {
    as: 'users',
    through: AdminServerModel,
    foreignKey: 'accountId',
    constraints: false,
});

// UserAccount -> 1:n -> User
UserAccountModel.hasMany(UserModel, {
    foreignKey: 'id',
    sourceKey: 'userId',
    constraints: false,
});

// UserAccount -> 1:n -> Account
UserAccountModel.hasMany(AccountModel, {
    foreignKey: 'id',
    sourceKey: 'accountId',
    constraints: false,
});

AdminModel.belongsToMany(ServerModel, {
    as: 'servers',
    through: AdminServerModel,
    foreignKey: 'adminId',
    constraints: false,
});

ServerModel.belongsToMany(AdminModel, {
    as: 'admins',
    through: AdminServerModel,
    foreignKey: 'serverId',
    constraints: false,
});

AdminServerModel.hasMany(AdminModel, {
    foreignKey: 'id',
    sourceKey: 'adminId',
    constraints: false,
});

AdminServerModel.hasMany(ServerModel, {
    foreignKey: 'id',
    sourceKey: 'serverId',
    constraints: false,
});

module.exports = {
	UserModel,
    AccountModel,
    AdminModel,
    ServerModel,
    UserAccountModel,
    AdminServerModel,
};
