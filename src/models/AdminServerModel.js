module.exports = function(sequelize, DataTypes) {
	return sequelize.define('AdminServerModel', {
		adminId: {
			type: DataTypes.UUID,
			primaryKey: false,
		},
		serverId: {
			type: DataTypes.UUID,
			primaryKey: false,
		},
		accessId: {
			type: DataTypes.UUID,
			primaryKey: false,

		},
		expired: DataTypes.DATE,
	}, {
		timestamps: true,
		underscored: false,
		tableName: 'admins_servers',
		indexes: [{
			unique: true,
			fields: ['adminId', 'serverId']
		}]
	});
};
