module.exports = function(sequelize, DataTypes) {
	return sequelize.define('AccountModel', {
		id: {
			allowNull: false,
			autoIncrement: false,
			primaryKey: true,
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
		},
		ownerId: {
            type: DataTypes.UUID,
            primaryKey: false,
		}
	}, {
		timestamps: true,
		underscored: false,
		tableName: 'accounts',
	});
};


