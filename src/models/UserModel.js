module.exports = function(sequelize, DataTypes) {
	return sequelize.define('UserModel', {
		id: {
			allowNull: false,
			autoIncrement: false,
			primaryKey: true,
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
		},
		provider: {
			type: DataTypes.STRING(10),
			allowNull: true,
		},
		displayName: {
			type: DataTypes.STRING(64),
			allowNull: true,
		},
		email: {
			type: DataTypes.STRING(255),
			unique: true,
			allowNull: false,
            validate: {
				isEmail: true,
                isUnique: (value, next) => {
                    sequelize.models.UserModel
						.count({
							where: {
								email: value
							}
						})
                        .then(count => {
                            if (count > 0) {
                                return next('user already exists!');
                            }
                            return next();
                        })
                        .catch(function (err) {
                            return next(err);
                        });
                }
            }
		},
		password: {
			type: DataTypes.STRING(64),
			allowNull: false,
		},
		role: {
			type: DataTypes.STRING(15),
			allowNull: true,
			default: 'user'
		}
	}, {
		timestamps: true,
		underscored: false,
		tableName: 'users',
		// instanceMethods: {
		// 	toJSON: function()
		// 	{
		// 		return utilities.removeSnakeAttributes(this.get());
		// 	}
		// }
	});
};


