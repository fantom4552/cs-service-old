module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ServerModel', {
		id: {
			allowNull: false,
			autoIncrement: false,
			primaryKey: true,
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
		},
        accountId: {
            type: DataTypes.UUID,
            primaryKey: false,
        },
		title: {
			type: DataTypes.STRING(64),
		},
		address: {
			type: DataTypes.STRING(25),
			unique: true,
			validate: {
			    isUnique: (value, next) => {
					sequelize.models.ServerModel
						.count({
							where: {
								address: value
							}
						})
			            .then((count) => {
			                if (count > 0) {
			                    return next('server already exists!');
			                }
			                return next();
			            })
			            .catch(function (err) {
			                return next(err);
			            });
			    }
			}
		}
	}, {
		timestamps: true,
		underscored: false,
		tableName: 'servers',
	});
};
