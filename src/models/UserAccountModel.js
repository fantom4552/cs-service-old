module.exports = function(sequelize, DataTypes) {
	return sequelize.define('UserAccountModel', {
		userId: {
			type: DataTypes.UUID,
			primaryKey: false,
		},
		accountId: {
			type: DataTypes.UUID,
			primaryKey: false,
		},
		permissions: {
			type: DataTypes.INTEGER,
			primaryKey: false,
		}
	}, {
		timestamps: true,
		underscored: false,
		tableName: 'users_accounts',
		indexes: [{
			unique: true,
			fields: ['userId', 'serverId']
		}]
	});
};
