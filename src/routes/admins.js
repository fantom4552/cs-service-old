const express = require('express');

const checkAuth = require('../middlewares/SecurityMiddleware').checkAuth;
const ParamsMiddleware = require('../middlewares/ParamsMiddleware');
const ControllerHelper = require('../helpers/ControllerHelper');
const AdminsController = require('../controllers/AdminsController');

const router = express.Router();

router.use(checkAuth);

ParamsMiddleware.RouteParamAdminId(router);
ParamsMiddleware.RouteParamServerId(router);

// TODO: Check permissions in middleware
const notFoundRoute = ControllerHelper.error('Not found', 404);
router.route('/')
    .get(ControllerHelper.route(AdminsController.getList))
    .post(ControllerHelper.route(AdminsController.create))
    .put(notFoundRoute)
    .delete(notFoundRoute);

router.route('/:adminId')
    .get(ControllerHelper.route(AdminsController.getItem))
    .post(notFoundRoute)
    .put(ControllerHelper.route(AdminsController.update))
    .delete(ControllerHelper.route(AdminsController.remove));

router.route('/:adminId/servers')
    .get(ControllerHelper.route(AdminsController.getServers))
    .post(notFoundRoute)
    .put(notFoundRoute)
    .delete(notFoundRoute);

router.route('/:adminId/servers/:serverId')
    .get(ControllerHelper.route(AdminsController.getServer))
    .post(notFoundRoute)
    .put(ControllerHelper.route(AdminsController.addServer))
    .delete(ControllerHelper.route(AdminsController.removeServer));

module.exports = router;
