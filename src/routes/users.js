const express = require('express');
const ControllerHelper = require('../helpers/ControllerHelper');
const UsersController = require('../controllers/UsersController');

const router = express.Router();
// TODO: Check permissions in middleware
router.post('/register', ControllerHelper.route(UsersController.register));
router.post('/login', ControllerHelper.route(UsersController.login));
router.get('/logout', ControllerHelper.route(UsersController.logout));

module.exports = router;
