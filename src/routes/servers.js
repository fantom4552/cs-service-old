const express = require('express');

const checkAuth = require('../middlewares/SecurityMiddleware').checkAuth;
const ParamsMiddleware = require('../middlewares/ParamsMiddleware');
const ControllerHelper = require('../helpers/ControllerHelper');
const ServersController = require('../controllers/ServersController');

const router = express.Router();
router.use(checkAuth);
ParamsMiddleware.RouteParamAdminId(router);

const notFoundRoute = ControllerHelper.error('Not found', 404);
router.route('/')
    .get(ControllerHelper.route(ServersController.getList))
    .post(ControllerHelper.route(ServersController.create))
    .put(notFoundRoute)
    .delete(notFoundRoute);

router.route('/:serverId')
    .get(ControllerHelper.route(ServersController.getItem))
    .post(notFoundRoute)
    .put(ControllerHelper.route(ServersController.update))
    .delete(ControllerHelper.route(ServersController.remove));

module.exports = router;
