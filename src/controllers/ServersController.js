const _ = require('lodash');
const ResponseHelper = require('../helpers/ResponseHelper');
const ServerModel = require('../models').ServerModel;

class ServersController {
	static getList(req) {
		return ServerModel
			.findAll()
            .then(servers => {
                return ResponseHelper
                    .json()
                    .setData(servers);
            });
	}

	static getItem(req) {
        const response = ResponseHelper
            .json()
            .setData(req.server);

        return Promise.resolve(response);
	}

	static create(req) {
        const attributes = _.pick(req.body, ['title', 'address']);
		return ServerModel
			.create(attributes)
			.then(server => server.save())
            .then(server => {
                return ResponseHelper
                    .json()
                    .setData(server);
            });
	}

	static update(req) {
        const attributes = _.pick(req.body, ['title']);
		return req.server.update(attributes, {fields: Object.keys(attributes)})
            .then(server => {
                return ResponseHelper
                    .json()
                    .setData(server);
            });
	}

	static remove(req) {
		return req.server.destroy()
            .then(() => {
                return ResponseHelper
                    .json()
                    .setData({
                        success: true
                    });
            });
	}
}
module.exports = ServersController;
