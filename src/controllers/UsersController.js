const _ = require('lodash');
const ResponseHelper = require('../helpers/ResponseHelper');
const SecurityHelper = require('../helpers/SecurityHelper');
const UserModel = require('../models').UserModel;
const AccountModel = require('../models').AccountModel;

class UsersController {
    static async register(req) {
        const attributes = _.pick(req.body, ['email', 'password', 'displayName']);
        attributes.provider = 'local';

        const user = await UserModel.create(attributes);
        await AccountModel.create({
			ownerId: user.id
		});

        return ResponseHelper
			.json()
			.setData(user);
	}

	static async login(req) {
		const user = await UserModel.find({
			where: {
				email: req.body.email
			}
		});

		if (!user) {
			throw new Error('Incorrect email');
		}

		const matched = await SecurityHelper.passwordVerify(req.body.password, user.password);
		if (!matched) {
            throw new Error('Incorrect password');
		}
		const token = await SecurityHelper.tokenGenerate({
			id: user.id,
			friendlyName: user.friendlyName
		}, process.env.JWT_SECRET);

        return ResponseHelper
            .json()
            .setCookie('jwt', token, {
                // TODO: expires
                httpOnly: true
            })
            .setData({
                success: true
            });
    }

    static logout() {
    	const response = ResponseHelper
			.json()
			.removeCookie('jwt')
			.setData({
				success: true
			});

    	return Promise.resolve(response);
	}
}

module.exports = UsersController;
