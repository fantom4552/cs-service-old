const _ = require('lodash');
const ResponseHelper = require('../helpers/ResponseHelper');
const AdminModel = require('../models').AdminModel;
const AdminServerModel = require('../models').AdminServerModel;

class AdminsController {
	static getList(req) {
		// TODO: filter by owner
		return AdminModel
			.findAll()
			.then(admins => {
				return ResponseHelper
					.json()
					.setData(admins);
			});
	}

	static getItem(req) {
        const response = ResponseHelper
            .json()
            .setData(req.admin);

        return Promise.resolve(response);
	}

	static create(req) {
        const attributes = _.pick(req.body, ['nickname', 'authId', 'password']);
		return AdminModel
			.create(attributes)
            .then(admin => {
                return ResponseHelper
                    .json()
                    .setData(admin);
            });
	}

	static update(req) {
        const attributes = _.pick(req.body, ['nickname', 'authId', 'password']);
		return req.admin.update(attributes, {fields: Object.keys(attributes)})
            .then(admin => {
                return ResponseHelper
                    .json()
                    .setData(admin);
            });
	}

	static remove(req) {
		return req.admin.destroy()
			.then(() => {
                return ResponseHelper
                    .json()
                    .setData({
                        success: true
                    });
            });
	}

	static getServers(req) {
		return req.admin.getServers();

	}

	static getServer(req) {
		return AdminServerModel
			.find({
				where: {
					adminId: req.admin.id,
					serverId: req.server.id,
				}
			})
			.then(adminServer => {
				if (!adminServer) {
					throw new Error('Not found');
				}
				return ResponseHelper
					.json()
					.setData(adminServer);
			});
	}

	static addServer(req) {
		return AdminServerModel
			.findOrCreate({
				where: {
					adminId: req.admin.id,
					serverId: req.server.id,
				},
				defaults: {
					adminId: req.admin.id,
					serverId: req.server.id,
					expired: Date.now()
				}
			})
			.spread((adminServer, created) => {
				if (created) {
					return adminServer;
				} else {
					return adminServer.update({
						expired: Date.now()
					}, {
						fields: ['expired']
					});
				}
			})
			.then(() => {
				return ResponseHelper
					.json()
					.setData({
						success: true
					});
			});
	}

	static removeServer(req) {
		// TODO: Better is to use AdminServersModel
        return AdminServerModel
			.find({
				adminId: req.params.adminId,
                serverId: req.params.serverId
            })
			.then(adminServer => {
				if (!adminServer) {
					throw new Error('Not found');
				}
				return adminServer.destroy();
			})
			.then(() => {
                return ResponseHelper
                    .json()
                    .setData({
                        success: true
                    });
			});
	}
}
module.exports = AdminsController;
