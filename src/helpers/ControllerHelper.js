const ResponseHelper = require('./ResponseHelper');

class ControllerHelper {
	static route(fn) {
		return (req, res) => {
            fn(req)
				.then(data => {
					if (data instanceof ResponseHelper) {
						data.render(res);
					} else {
                        res.json(data);
                    }
                })
				.catch(error => {
					res
						.status(500)
                    	.json({
							error: error.message
						});
                    console.error(error)
				});
		}
	}

	static error(message, status = 404) {
		return (req, res, next) => {
			next({
				message,
				status
			});
		}
	}
}

module.exports = ControllerHelper;
