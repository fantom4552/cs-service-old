const Sequelize = require('sequelize');
const config = require('config');
const sequelize = new Sequelize(config.get('DataBase.name'), config.get('DataBase.user'), config.get('DataBase.pass'), config.get('DataBase.params'));

module.exports = sequelize;
