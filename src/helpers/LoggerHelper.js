const winston = require('winston');
const config = require('config');

winston.clear();

const transports = config.get('Logger.transports');
if (transports.includes('console')) {
	winston.add(winston.transports.Console, {
		level: config.get('Logger.level'),
		silent: false,
		colorize: true,
		timestamp: true,
		json: false,
		stringify: true,
		prettyPrint: true,
		depth: 3,
		humanReadableUnhandledException: true,
		showLevel: true,
		handleExceptions: true,
	});
}

module.exports = winston;
