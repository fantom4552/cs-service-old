const TYPE_JSON = 'json';

class ResponseHelper {
    constructor(type) {
        this.type = type;
        this.status = 200;
        this.data = null;
        this.headers = [];
        this.cookies = [];
    }

    /**
     *
     * @param status number
     */
    setStatus(status) {
        this.status = status;
    }

    /**
     *
     * @param data
     * @returns {ResponseHelper}
     */
    setData(data) {
        this.data = data;
        return this;
    }

    /**
     *
     * @param name string
     * @param value string
     * @returns {ResponseHelper}
     */
    setHeader(name, value) {
        this.cookies.push({
            name,
            value
        });
        return this;
    }

    /**
     *
     * @param name string
     * @param value string
     * @param options object
     * @returns {ResponseHelper}
     */
    setCookie(name, value, options = {}) {
        this.cookies.push({
            name,
            value,
            options
        });
        return this;
    }

    removeCookie(name) {
        let founded = false;
        this.cookies.forEach((val, key) => {
            if (val.name === name) {
                this.cookies[key].value = null;
                founded = true;
            }
        });

        if (!founded) {
            this.cookies.push({
                name,
                value: null
            });
        }
        return this;
    }

    render(res) {
        res.status(this.status);

        this.headers.forEach((header) => {
            res.set(header.name, header.value);
        });

        this.cookies.forEach((cookie) => {
            if (cookie.value) {
                res.cookie(cookie.name, cookie.value, cookie.options);
            } else {
                res.clearCookie(cookie.name);
            }
        });

        switch (this.type) {
            case TYPE_JSON:
                res.json(this.data);
                break;

            default:
                res.send(this.data);
        }
    }

    /**
     *
     * @returns {ResponseHelper}
     */
    static json() {
        return new ResponseHelper(TYPE_JSON);
    }
}

module.exports = ResponseHelper;
