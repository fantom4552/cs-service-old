const request = require('supertest');
const sinon = require('sinon');
// const assert = require('chai').assert;
const app = require('../../src/app');
const ControllerHelper = require('../../src/helpers/ControllerHelper');

describe('Routes', () => {
	let sandbox;
	before(() => {
		sandbox = sinon.sandbox.create();
		sandbox.stub(ControllerHelper, 'route').callsFake(() => (req, res) => {
			res.status(200);
			res.json({});
		});
	});
	after(() => {
		sandbox.restore();
	});

	const tests = [
		{ name: 'Test login route', method: 'post', url: '/login' },
		{ name: 'Test register route', method: 'post', url: '/register' },
		{ name: 'Test logout route', method: 'get', url: '/logout' },
	];
	tests.forEach(test => {
		it(test.name, function(done) {
			request(app)[test.method](test.url)
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200, done);
		});
	})

});
