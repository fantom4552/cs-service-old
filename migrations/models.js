const path = require('path');
const modelsPath = path.resolve(__dirname, '../src/models');

const sequelize = require('../src/helpers/DatabaseHelper');

const UserModel = sequelize.import(`${modelsPath}/UserModel`);
const AccountModel = sequelize.import(`${modelsPath}/AccountModel`);
const AdminModel = sequelize.import(`${modelsPath}/AdminModel`);
const ServerModel = sequelize.import(`${modelsPath}/ServerModel`);
const AdminServerModel = sequelize.import(`${modelsPath}/AdminServerModel`);
const UserAccountModel = sequelize.import(`${modelsPath}/UserAccountModel`);

const options = {
	collate: ' utf8mb4_unicode_ci',
};

module.exports = {
	up: function (queryInterface) {
		return queryInterface.createTable(UserModel.tableName, UserModel.attributes, options)
			.then(() => queryInterface.createTable(AccountModel.tableName, AccountModel.attributes, options))
			.then(() => queryInterface.createTable(AdminModel.tableName, AdminModel.attributes, options))
			.then(() => queryInterface.createTable(ServerModel.tableName, ServerModel.attributes, options))
			.then(() => queryInterface.createTable(UserAccountModel.tableName, UserAccountModel.attributes, options))
			.then(() => queryInterface.createTable(AdminServerModel.tableName, AdminServerModel.attributes, options));
	},
	down: function (queryInterface) {
		return queryInterface.dropTable(UserModel.tableName)
			.then(() => queryInterface.dropTable(AccountModel.tableName))
			.then(() => queryInterface.dropTable(AdminModel.tableName))
			.then(() => queryInterface.dropTable(ServerModel.tableName))
			.then(() => queryInterface.dropTable(UserAccountModel.tableName))
			.then(() => queryInterface.dropTable(AdminServerModel.tableName));
	}
};
