import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { adminsLoad } from '../../actions/admins'

import AdminsTable from '../../components/admins/AdminsList';

const mapStateToProps = (state, props) => {
	return {
		admins: state.admins.data,
		isLoading: state.admins.isLoading,
		isLoaded: state.admins.isLoaded,
	}
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({ adminsLoad }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminsTable);
