import { reduxForm } from 'redux-form';

const FormContainer = ({ component, name, action }) => {
    return reduxForm({
        form: name,
        validate: (values) => {
            const errors = {};
            if (!values.address) {
                errors.address = 'Required';
            } else if (/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{3,5}$/.test(values.address) === false) {
                errors.address = 'Bad Ip address';
            }

            return errors;
        },
        onSubmit: (values, dispatch) => dispatch({
            type: action,
            payload: values
        })
    })(component);
};

export default FormContainer;
