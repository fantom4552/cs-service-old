import { indigo, cyan, red } from 'material-ui/colors';

const getTheme = () => {
	return {
		palette: {
			primary: indigo,
			secondary: cyan,
			error: red,
		},
	}
};

export default getTheme;
