import React from 'react';
import { FormControl } from 'material-ui/Form';
import { withStyles } from 'material-ui/styles';

import FormContainer from '../../containers/forms';
import { TextElement, SubmitButton } from '../../forms/elelemts';


const styles = theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

const AdminsCreate = (props) => {
    const {handleSubmit, pristine, submitting, classes} = props;
    return (
        <form onSubmit={handleSubmit} className={classes.container}>
            <FormControl className={classes.formControl}>
                <TextElement name="address" label="Server IP" />
            </FormControl>
            <FormControl className={classes.formControl}>
                <SubmitButton pristine={pristine} submitting={submitting} color="primary" raised>
                    Save
                </SubmitButton>
            </FormControl>
        </form>
    );
};

// https://stackoverflow.com/a/43046662
export default withStyles(styles)(FormContainer({
    component: AdminsCreate,
    name: 'AdminsCreateForm',
    action: 'ADMINS_CREATE_SEND'
}));

