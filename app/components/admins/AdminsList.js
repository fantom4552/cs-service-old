import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loader';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Icon from 'material-ui/Icon';

import Dialog from 'material-ui/Dialog';
import IconButton from 'material-ui/IconButton';
import AddCircleIcon from 'material-ui-icons/AddCircle';

import AdminsCreate from './AdminsCreate';

class AdminsTable extends Component {
	state = {
		open: false,
	};

	handleClickOpen = () => {
		this.setState({ open: true });
	};

	handleRequestClose = () => {
		this.setState({ open: false });
	};

	componentDidMount() {
		this.props.adminsLoad();
	}
	render() {
		return (
			<Loader loaded={this.props.isLoaded}>
				<Paper>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell>Nickname</TableCell>
								<TableCell>Auth ID</TableCell>
								<TableCell/>
							</TableRow>
						</TableHead>
						<TableBody>
							{this.props.admins.map(row => {
								return (
									<TableRow key={row.id}>
										<TableCell>{row.nickname}</TableCell>
										<TableCell>{row.authId}</TableCell>
										<TableCell>
											<Icon onClick={() => {alert(1)}} style={{ cursor: 'pointer' }}>add_circle</Icon>
										</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</Paper>

				<IconButton color="primary" onClick={this.handleClickOpen} aria-label="Close">
					<AddCircleIcon style={{
						width: 36,
						height: 36,
					}} />
				</IconButton>
				<Dialog open={this.state.open} onRequestClose={this.handleRequestClose}>
					<AdminsCreate/>
				</Dialog>
			</Loader>
		);
	};
}

AdminsTable.propTypes = {
	admins: PropTypes.array,
	isLoaded: PropTypes.bool,
	adminsLoad: PropTypes.func,
};

AdminsTable.defaultProps = {
	admins: [],
	isLoaded: false,
};

export default AdminsTable;
