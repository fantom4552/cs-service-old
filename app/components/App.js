import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { Route, Switch } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


import Grid from 'material-ui/Grid';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
import Hidden from 'material-ui/Hidden';
import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';

import AdminsList from '../containers/admins/AdminsList';
import AdminsCreate from './admins/AdminsCreate';

let App = ({classes, goToCreate}) => {
	const drawer = (
		<div>
			<Hidden smUp implementation="css">
				<IconButton className={classes.drawerHeader}>
					<ChevronLeftIcon />
				</IconButton>
				<Divider />
			</Hidden>
			<List>
				<ListItem button onClick={goToCreate}>
					<ListItemIcon>
						<MenuIcon />
					</ListItemIcon>
					<ListItemText primary="Drafts" />
				</ListItem>
			</List>
		</div>
	);

	return (
		<div>
			<AppBar position="static">
				<Toolbar disableGutters={true}>
					<Hidden smUp implementation="css">
						<IconButton color="contrast" aria-label="Menu">
							<MenuIcon />
						</IconButton>
					</Hidden>
					<Typography type="title" color="inherit">
						Title
					</Typography>
				</Toolbar>
			</AppBar>
			<Hidden smDown implementation="css">
				<Drawer type="permanent" open={false} classes={{paper: classes.paper}}>{drawer}</Drawer>
			</Hidden>
			<Hidden smUp implementation="css">
				<Drawer type="permanent" open={false} classes={{paper: classes.paper}}>{drawer}</Drawer>
			</Hidden>
			<Grid> container>
				<Grid item xs={12} sm={12} md={12} lg={12}>
					<main className={classes.main}>
						<Switch>
							<Route path="/" exact component={AdminsList} />
							<Route path="/create" exact component={AdminsCreate} />
						</Switch>
					</main>
				</Grid>
			</Grid>
		</div>
	);
};

App.propTypes = {
    classes: PropTypes.object.isRequired,
};

const goToCreate = () => {
	return (dispatch) => {
		dispatch(push('/create'));
	};
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({ goToCreate }, dispatch)
};

export default connect(null, mapDispatchToProps)(App);


