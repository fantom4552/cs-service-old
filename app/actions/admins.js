import {ADMINS_IS_LOADING, ADMINS_IS_LOADED, ADMINS_HAS_ERROR} from '../constants/admins';

const adminsIsLoading = () => {
	return {
		type: ADMINS_IS_LOADING
	};
};

const adminsIsLoaded = (data) => {
	return {
		type: ADMINS_IS_LOADED,
		payload: data
	};
};

const adminsHasError = (error) => {
	return {
		type: ADMINS_HAS_ERROR,
		payload: error
	};
};

const adminsLoad = () => {
	return (dispatch) => {
		dispatch(adminsIsLoading());

		fetch('/api/v1/admins')
			.then(response => response.json())
			.then(data => dispatch(adminsIsLoaded(data)));
			// .catch(error => dispatch(adminsHasError(error)));
	};
};

export {
	adminsIsLoading,
	adminsIsLoaded,
	adminsHasError,
	adminsLoad
}
