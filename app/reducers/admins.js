import {ADMINS_IS_LOADING, ADMINS_IS_LOADED, ADMINS_HAS_ERROR} from '../constants/admins';

const initialState = {
	admins: {
		isLoaded: false,
		error: null,
		data: []
	}
};

const adminsReducer = (state = initialState, action = null) => {
	switch (action.type) {
		case ADMINS_IS_LOADING:
			return {
				isLoaded: false,
				error: null,
				data: []
			};
		case ADMINS_IS_LOADED:
			return {
				isLoaded: true,
				error: null,
				data: action.payload
			};
		case ADMINS_HAS_ERROR:
			return {
				isLoaded: true,
				error: action.payload,
				data: []
			};
		case 'ADMINS_CREATE_SEND':
			return {
                isLoaded: false,
                error: null,
                data: []
			};
		default:
			return state
	}
};

export default adminsReducer;
