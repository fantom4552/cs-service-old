import React from 'react';
import { Field } from 'redux-form';

import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';

const renderTextField = ({input, label, meta, ...custom}) => {
    return (
        <TextField
            id="name"
            label={label}
            error={!!(meta.touched && meta.error)}
            helperText={meta.touched && meta.error}
            {...input}
            {...custom}
        />
    );
};

const TextElement = (props) => {
    return <Field component={renderTextField} {...props} />;
};

const SubmitButton = (props) => {
    const {pristine, submitting, children, ...btnProps} = props;
    return (
        <Button type="submit" disabled={pristine || submitting} {...btnProps}>{children}</Button>
    )
};

export {
    TextElement,
    SubmitButton
};
