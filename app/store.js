import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import adminsReducer from './reducers/admins';

const getInitialState = (rootElement) => {
	let user;
	try {
		const data = JSON.parse(rootElement.dataset.user);
		user = data ? {
			id: data.id,
			isAuthenticated: true
		} : {
			id: 0,
			isAuthenticated: false
		};
	} catch(e) {
		console.error(e);
		user = {
			id: 0,
			isAuthenticated: false
		};
	}

	return {
		user,
		admins: {
			isLoaded: false,
			error: null,
			data: []
		}
	};
};

const history = createHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(
	applyMiddleware(thunk, routerMiddleware(history))
);

const userReducer = (state, action) => {
	if (typeof state === 'undefined') {
		return {
			id: 0,
			isAuthenticated: false
		};
	} else {
		return state;
	}
};

const rootReducer = combineReducers({
	admins: adminsReducer,
    form: formReducer,
	router: routerReducer,
	user: userReducer
});

export default (initialState) => {
	return createStore(
		rootReducer,
		initialState,
		enhancer,
	);
};

export {
	history,
	getInitialState
};
