import { withStyles } from 'material-ui/styles';

import App from '../components/App';

const styles = theme => ({
	root: {
		flexGrow: 1
	},
	paper: {
		top: 0,
		width: 150,
		[theme.breakpoints.up('sm')]: {
			top: 64
		},
	},
	drawerHeader: {
		height: 56
	},
	main: {
		marginLeft: 150,
		[theme.breakpoints.down('sm')]: {
			marginLeft: 0
		},
	}
});

export default withStyles(styles)(App);
