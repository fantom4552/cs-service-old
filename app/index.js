import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

import configureStore, { getInitialState, history } from './store';
import getTheme from './theme';

import App from './styles';

const root = document.getElementById('root');

const store = configureStore(getInitialState(root));
const theme = createMuiTheme(getTheme());

ReactDOM.render(
	<MuiThemeProvider theme={theme}>
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<App/>
			</ConnectedRouter>
		</Provider>
	</MuiThemeProvider>,
	root
);
