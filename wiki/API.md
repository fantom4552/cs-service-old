

```POST /api/register
Request: {
	email: '',
	password: ''
}
Response: {
	id: '',
	email: ''
}

POST /api/login
Request: {
	email: '',
	password: ''
}
Response: {
	success: true,
	id: ''
}

GET /api/logout
Response: {
	redirect: '/'
}

POST /api/password
Request: {
	oldPassword: '',
	password: ''
}
Response: {
	success: true
}

POST /api/settings
Request: {
	email: '',
	displayName: ''
}
Response: {
	success: true,
	email: '',
	displayName: ''
}


GET /api/v1/servers
Response: {
	accountId: {
		"owner": {
			"id": '',
			"displayName": ''
		}.
		"servers": {
			serverId: {
				"title": '',
				"address": ''
			}
		}
	}
}

POST /api/v1/servers
Request: {
	title: '',
	address: ''
}
Response: {
	id: '',
	title: '',
	addreess: ''
}

PUT /api/v1/servers/:serverId
Request: {
	title: ''
}
Response: {
	id: '',
	title: '',
	addreess: ''
}

DELETE /api/v1/servers/:serverId
Response: {
	success: true
}

GET /api/v1/servers/:serverId/flags
Response: {
	flagId: {
		title: '',
		flags: ''
	}
}

POST /api/v1/servers/:serverId/flags
Request: {
	title: '',
	flags: '',
}

PUT /api/v1/servers/:serverId/flags/:flagId
Request: {
	title: '',
	flags: '',
}
Response: [{
	id: '',
	title: '',
	flags: ''
}]

DELETE /api/v1/servers/:serverId/flags/:flagId
Response: {
	success: true
}

GET /api/v1/servers/:serverId/admins
Response: [{
	id: '',
	nickaname: '',
	authId: '',
	flags: {
		id: '',
		title: '',
		flags: ''
	},
	access: 0,
	expired: 0
}]

POST /api/v1/servers/:serverId/admins/:adminId
Request: {
	flags: '',
	expired: 0
}
Response: {
	id: '',
	nickaname: '',
	authId: '',
	flags: {
		id: '',
		title: '',
		flags: ''
	},
	access: 0,
	expired: 0
}

PUT /api/v1/servers/:serverId/admins/:adminId
Request: {
	access: '',
	expired: 0
}
Response: {
	id: '',
	nickaname: '',
	authId: '',
	flags: {
		id: '',
		title: '',
		flags: ''
	},
	access: 0,
	expired: 0
}

DELETE /api/v1/servers/:serverId/admins/:adminId
Response: {
	success: true
}

GET /api/v1/admins
Response: [{
	id: '',
	nickaname: '',
	authId: '',
	access: 0,
	servers: [{
		id: '',
		title: '',
		flags: {
			id: '',
			title: '',
			flags: ''
		},
		access: 0,
		expired: 0
	}]
}]

POST /api/v1/admins
Request: {
	nickaname: '',
	authId: '',
	access: 0
}
Response: {
	id: '',
	nickaname: '',
	authId: '',
	access: 0
}

PUT /api/v1/admins/:adminId
Request: {
	nickaname: '',
	authId: '',
	access: 0
}
Response: {
	id: '',
	nickaname: '',
	authId: '',
	access: 0
}

DELETE /api/v1/admins/:adminId
Response: {
	success: true
}
```
